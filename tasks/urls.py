from django.urls import path
from tasks.views import create_task, show_my_tasks
from accounts.views import logout_view

urlpatterns = [
    path("create/", create_task, name="create_task"),
    path("mine/", show_my_tasks, name="show_my_tasks"),
    path("accounts/login/", logout_view, name="logout_view"),
]
